<div class="well">
	<div class="media">
		<a class="pull-left" href="{poster}">
    	<img class="media-object img-rounded" src="{thumbnail}" height="100%">
    </a>
    <div class="media-body">
      <h4 class="media-heading">
      	<a href="https://www.themoviedb.org/movie/{tmdbid}">{title}</a> ({year})
        <span class="label label-danger">
         	<a href="{trailer}" class="glyphicon glyphicon-play"></a>
        </span>
      </h4>
      <span class="label label-warning">{markhtml}</span>
      <i class="text-muted">{alternative}</i><br/><br/>
			<div class="row">
  			<div class="col-md-6">
  				<span class="glyphicon glyphicon-book"></span>
  				<h5>{description}</h5>
  			</div>
  			<div class="col-md-6">
  				<span class="glyphicon glyphicon-user"></span>
  				<h5>{comment}</h5>
  			</div>
			</div>
		</div>
	</div>
</div>
