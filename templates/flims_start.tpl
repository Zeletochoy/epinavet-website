<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Epinavet</title>
    <link rel="icon" type="image/png" href="img/favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.css" media="screen">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../bower_components/html5shiv/dist/html5shiv.js"></script>
      <script src="../bower_components/respond/dest/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
  	
<nav class="navbar navbar-default" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.html"><img src="img/epinavet.png" height="100%"/></a>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="presentation.html"><span class="glyphicon glyphicon-book"></span> Présentation</a></li>
        <li><a href="programme.html"><span class=" glyphicon glyphicon-calendar"></span> Programme</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-film"></span> Flims<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="nanars.html">Nanars</a></li>
            <li><a href="immanquables.html">Immanquables</a></li>
            <li><a href="todo.html">À voir</a></li>
          </ul>
        </li>
        <li><a href="contact.html"><span class="glyphicon glyphicon-envelope"></span> Contact</a></li>
        <li><a href="liens.html"><span class="glyphicon glyphicon-link"></span> Liens</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container">

<h2>{00}</h2>
<hr>
