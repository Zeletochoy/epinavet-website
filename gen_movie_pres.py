#! /usr/bin/env python3

import sys
import tmdb
import difflib
from yaml import load, dump
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


tmdb.configure("2e9b950b3a78caab1132b5224f39c64a", "fr")

class Card:
    def __init__(self, title, mark, comment):
        sys.stderr.write("Searching for " + title + "\n")
        self.info = {}
        self.info["mark"] = mark
        self.info["comment"] = comment
        movies = tmdb.Movies(title)
        r = 0
        for m in movies.iter_results():
            sm = difflib.SequenceMatcher(None, title, m["title"])
            cr = sm.ratio()
            if cr > r:
                r = cr
                movie = m
        movie = tmdb.Movie(movie["id"])
        self.info["title"] = movie.get_original_title()
        no_orig = not self.info["title"]
        if no_orig:
            self.info["title"] = movie.get_title()
        self.info["description"] = movie.get_overview()
        self.info["thumbnail"] = movie.get_poster("s")
        self.info["poster"] = movie.get_poster("o")
        self.info["alternative"] = "" if no_orig else movie.get_title()
        for t in movie.get_alternative_titles():
            if t["lang"] == "FR":
                self.info["alternative"] = t["title"]
        try:
            trailer = movie.get_trailers()["youtube"][0]["source"]
            self.info["trailer"] = ("" if "youtube" in trailer \
                    else "http://www.youtube.com/watch?v=") + trailer
        except:
            self.info["trailer"] = "javascript:alert('indisponible')"
        self.info["year"] = movie.get_release_date()[:4]
        self.info["tmdbid"] = movie.get_id()
        self.info["markhtml"] = '<span class="glyphicon glyphicon-star"></span>' * mark
        self.info["markhtml"] += '<span class="glyphicon glyphicon-star-empty"></span>' * (5 - mark)

def load_html(path):
        f = open(path)
        html = f.read()
        f.close()
        return html

def load_cards(path):
    f = open(path)
    yml = load(f.read(), Loader=Loader)
    f.close()
    cards = []
    for m in yml:
        cards.append(Card(m, yml[m]["mark"], yml[m]["comment"]))
    return cards

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: {} pagename yaml".format(sys.argv[0]));
        sys.exit(1)
    start = load_html("templates/flims_start.tpl")
    card = load_html("templates/flims_card.tpl")
    end = load_html("templates/flims_end.tpl")
    print(start.format(sys.argv[1]))
    for c in load_cards(sys.argv[2]):
        print(card.format(**c.info))
    print(end)
