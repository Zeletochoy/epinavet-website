all: next todo flims nanars

nanars:
	./gen_movie_pres.py Nanars templates/nanars.yml > nanars.html

flims:
	./gen_movie_pres.py Flims templates/flims.yml > immanquables.html

todo:
	./gen_movie_pres.py "À voir" templates/todo.yml > todo.html

next:
	./gen_movie_pres.py "Prochaine soirée epinavet (`date --date="next Thursday" +%d/%m`)" templates/next.yml > programme.html
